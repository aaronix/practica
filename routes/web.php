<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\SupplierController;
use App\Models\Supplier;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'dash'])->name('dash');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('/dash', [HomeController::class, 'dash'])->name('dash');
Route::get('/proveedores', [SupplierController::class, 'index'])->name('supplier_index');
Route::get('/importar-proveedores', [SupplierController::class, 'import'])->name('import_suppliers');
Route::post('/importar-proveedores', [SupplierController::class, 'importStore'])->name('import_suppliers_store');
Route::get('/rfc', [HomeController::class, 'validarRFC'])->name('valida_rfc');
Route::get('/activar/{uuid}', [SupplierController::class, 'active'])->name('supplier_active');
