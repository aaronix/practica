<?php

namespace App\Http\Controllers;

use App\Imports\SuppliersImport;
use App\Models\Supplier;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;

class SupplierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title']='Proveedores';
        $data['section_title']='Listado de proveedores';
        $data['suppliers'] = Supplier::all();
        $user = Auth::user();
        
        if ($user->role_id==1) {            
            return view('supplier.index', compact('data'));
        }else{
            $data['user'] = $user;
            return view('supplier.info', compact('data'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supplier $supplier)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        //
    }

    public function import()
    {
        $data['title']='Proveedores';
        $data['section_title']='Importación de proveedores';
        return view('supplier.import', compact('data'));
    }
    
    public function importStore(Request $request)
    {
        
        $file_suppliers = $request->file('suppliers');
        if ($file_suppliers) {
            $path_xlsx = $file_suppliers->store('suppliers');
            $xlsx_name = $file_suppliers->getClientOriginalName();
            $xlsx = basename($path_xlsx);
            Excel::import(new SuppliersImport, $file_suppliers);
        }
        $status = 'success';
        $msg = 'Se ha importado el archivo';
        return redirect()->route('supplier_index')
        ->with('status',$status)
        ->with('msg',$msg);
        // return redirect('/')->with('success', 'All good!');
    }

    public function active($uuid)
    {
        
        $supplier = Supplier::where('uuid', $uuid)->get();
        $user_supplier = User::create([
            'name'      => $supplier[0]->name,
            'email'     => $supplier[0]->email,
            'password'  => Hash::make($supplier[0]->rfc),
            'role_id'   =>  2,
            'rfc'       => $supplier[0]->rfc
        ]);
        $supplier[0]->update(
            ['status'=>1]
        );
        $status = 'success';
        $msg = 'El proveedor <b>'.$supplier[0]->name.'</b> se a activado, ahora tiene acceso con el usuario <b>'.
        $supplier[0]->email.'</b> y la contraseña <b>'.$supplier[0]->rfc.'</b>';
        
        return redirect()->route('supplier_index')
        ->with('status',$status)
        ->with('msg',$msg);
        
    }
}
