<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dash()
    {
        $data['title']='Proveedores';
        $data['section_title']='Listado de proveedores';
        $user = Auth::user();
        $data['suppliers'] = Supplier::all();
        if ($user->role_id==1) {
            return view('supplier.index', compact('data'));
        }else{
            $data['user'] = $user;
            return view('supplier.info', compact('data'));
        }
        
    }
    
}
