<?php

namespace App\Imports;

use App\Models\Supplier;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Str;


class SuppliersImport implements ToModel, WithStartRow
{

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        return new Supplier([
            'uuid'          => Str::uuid(),
            'name'          => $row[0],
            'rfc'           => $row[1],
            'email'         => $row[2],
            'rfc_valid'     => $this->validarRFC($row[1]),
            'email_valid'   => $this->validarEmail($row[2]),
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }

    public function validarRFC($rfc='') {
        $regex = '/^[A-Z]{3,4}([0-9]{2})(1[0-2]|0[1-9])([0-3][0-9])([A-Z0-9]{3,4})$/';
        return preg_match($regex, $rfc);
    }

    public function validarEmail($email)
    {
        $emailFound = User::where('email', $email)->count();
        if ($emailFound==0 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }
    
    
}
