<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_admin = User::create([
            'name'      => 'admin',
            'email'     => 'admin@practica.com',
            'password'  => Hash::make('4dm1n'),
            'role_id'   =>  1
        ]);

        $user_supplier = User::create([
            'name'      => 'proveedor',
            'email'     => 'proveedor@practica.com',
            'password'  => Hash::make('proveedor'),
            'role_id'   =>  2
        ]);
    }
}
