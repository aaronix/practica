@extends('layout-default')
@section('body')
<h2 class="section-title">{{$data['section_title']??''}}</h2>
<p class="section-lead">Administración de proveedores</p>
<div class="card">
    <div class="card-header">
        <h4>Validación de los proveedores que tendran acceso a la paltaforma</h4>
        <div class="card-header-action">
            <a href="{{route('import_suppliers')}}" class="btn btn-primary">
              Importar
            </a>
          </div>
    </div>
    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-{{ session('status') }}
                alert-dismissible show fade">
                <div class="alert-body">
                    <button class="close" data-dismiss="alert">
                        <span>&times;</span>
                    </button>
                    {!! session('msg') !!}
                </div>
            </div>
        @endif
        <div class="table-responsive">
            <table class="table table-striped" id="table-1">
              <thead>
                <tr>
                  <th class="text-center">
                    #
                  </th>
                  <th>Nombre completo del proveedor</th>
                  <th>RFC</th>
                  <th>EMAIL</th>                  
                  <th>Estatus</th>
                  <th>Validar</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($data['suppliers'] as $supplier)
                    <tr>
                        <td>{{$loop->index+1}}</td>
                        <td>{{$supplier->name}}</td>
                        <td>{{$supplier->rfc}}
                            @if (!$supplier->rfc_valid)
                                <div class="badge badge-warning">RFC no válido</div>
                            @endif
                        </td>
                        <td>{{$supplier->email}}
                            @if (!$supplier->email_valid)
                            <div class="badge badge-warning">EMAIL repetido y/o no válido</div>
                            @endif
                        </td>
                        <td>
                            @if ($supplier->status)                                
                            <div class="badge badge-info">Validado</div>    
                            @elseif($supplier->rfc_valid && $supplier->email_valid)
                            <div class="badge badge-success">Válido</div>    
                            @else
                            <div class="badge badge-danger">No válido</div>                                
                            @endif
                        </td>
                        <td>
                            @if ($supplier->email_valid && ($supplier->status==0))
                            <a class="btn btn-primary" href="{{route('supplier_active', ['uuid'=>$supplier->uuid])}}" role="button">Validar</a>                                
                            @endif
                        </td>
                    </tr>                      
                  @endforeach                  
              </tbody>
            </table>
          </div>
        
    </div>
    <div class="card-footer bg-whitesmoke">
        <small class="text-danger">
            No se puede validar a un provedor si ya existe el email
        </small> 
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    // var asset_global='{{asset("/")}}';

    $( document ).ready(function() {

        $("#table-1").dataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json",
            },
            "columnDefs": [
                // { "sortable": false, "targets": [2,3] }
            ]
        });
    });      
</script>
@endpush
