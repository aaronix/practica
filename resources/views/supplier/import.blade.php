@extends('layout-default')
@section('body')
<h2 class="section-title">{{$data['section_title']??''}}</h2>
<p class="section-lead">{{Auth::user()->name}}</p>
<div class="card">
    <div class="card-header">
        <h4>Selecciona un archivo</h4>            
    </div>
    <form action="{{route('import_suppliers_store')}}" method="POST" id="form" enctype="multipart/form-data">        
        @csrf
    <div class="card-body">
        
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile" name="suppliers">
                <label class="custom-file-label" for="customFile">Selecciona un archivo</label>
            </div>

            
        
    </div>
    <div class="card-footer bg-whitesmoke">
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
    </form>
</div>
@endsection