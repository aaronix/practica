@extends('layout-default')
@section('body')
<h2 class="section-title">Información Usuario</h2>
<p class="section-lead">{{Auth::user()->name}}</p>
<div class="card">
    <div class="card-header">
        <h4>id => {{Auth::user()->id}}</h4>
    </div>
    <div class="card-body">
        <p>
            @dump(Auth::user()->toArray())
        </p>
        <div class="section-title mt-0">Empresa</div>
        <p>
            empresa-nombre
        </p>
        <div class="section-title mt-0">Rol
            <div class="badge badge-primary">Master</div>
            <div class="badge badge-primary">Supervisor</div>
            <div class="badge badge-primary">Administrador</div>
            <div class="badge badge-primary">Vendedor</div>
        </div>
        
        <p>
            rol-nombre
        </p>
        <div class="section-title mt-0">Puntos de venta (sucursales)</div>
        <p>
            puntos_venta-total
        </p>
        <div class="section-title mt-0">Usuarios</div>
        <p>
            usuarios-total
        </p>
    </div>
    <div class="card-footer bg-whitesmoke">
        This is card footer
    </div>
</div>
@endsection