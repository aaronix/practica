
## Para iniciar el sistema
1.-Ejecuta

git clone https://aaronix@bitbucket.org/aaronix/practica.git

2.-Ejecuta

composer install

3.-Copia el .env

cp .env.example .env ó copia .env.example .env

4.-Ejecuta

php artisan key:generate

5.-Crea una base de datos, llena los datos en el .env y ejecuta

php artisan migrate --seed

6.-Ejecuta

php artisan serve

7.-Para entrar al sistema el usuario Administrador es 

Usuario:

admin@practica.com

Password:

4dm1n

y el usuario Proveedor

Usuario:

proveedor@practica.com

Password:

proveedor

## Nota

anexo los archivos con los que se hicieron PRUEBAS

PRUEBAS\practicaXLS.xls

PRUEBAS\practicaCSV.csv

PRUEBAS\practicaTEXTO.csv



